package keyword.test;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Properties;
import org.apache.log4j.xml.DOMConfigurator;
import org.testng.Assert;
import org.testng.annotations.Test;
import utility.ExcelUtils;
import utility.Log;
import config.ActionKeywords;
import config.Constants;
import executionEngine.DriverScript;

public class KeywordTest { 
	public static Properties OR;
	public static ActionKeywords actionKeywords;
	public static String sActionKeyword;
	public static String sPageObject;
	public static Method method[];
	public static int iTestStep;
	public static int iTestLastStep;
	public static String sTestCaseID;
	public static String sRunMode;
	public static String sData;
	public static boolean bResult;
	
	@Test
	public void DriverScript() throws Exception {
		// Get all methods in ActionsKeywords class
		actionKeywords = new ActionKeywords();
		method = actionKeywords.getClass().getMethods();
    	
		// Get the absolute path of the project
    	String projectPath = new File("").getAbsolutePath();
		projectPath = projectPath.replace("\\", "/");
		Constants.Path_TestData = projectPath + "/src/main/java/dataEngine/DataEngine.xlsx"; 
		Constants.Path_OR = projectPath + "/src/main/java/config/OR.txt";
		
		// Set excel data
    	ExcelUtils.setExcelFile(Constants.Path_TestData);
    	
    	// DOMConfigurator is used to configure logger from xml configuration file
    	DOMConfigurator.configure("log4j.xml");
    	
    	// Load object repository properties file
		FileInputStream fs = new FileInputStream(Constants.Path_OR);
		OR = new Properties(System.getProperties());
		OR.load(fs);
		
		// Execute the test cases
		DriverScript startEngine = new DriverScript();
		startEngine.execute_TestCase();
		Assert.assertEquals(bResult, true);
    }
		
    public void execute_TestCase() throws Exception {
    	// Count the test cases
    	int iTotalTestCases = ExcelUtils.getRowCount(Constants.Sheet_TestCases);
    	
    	// Run test case
		for(int iTestcase=1; iTestcase < iTotalTestCases; iTestcase++) {
			// Set TestCaseID, RunMode and Result
			bResult = true;
			sTestCaseID = ExcelUtils.getCellData(iTestcase, Constants.Col_TestCaseID, Constants.Sheet_TestCases); 
			sRunMode = ExcelUtils.getCellData(iTestcase, Constants.Col_RunMode,Constants.Sheet_TestCases);
			
			// Run test case with RunMode "Yes"
			if(sRunMode.equals("Yes")) {
				Log.startTestCase(sTestCaseID);
				
				// Get started step and lastest step in the test case 
				iTestStep = ExcelUtils.getRowContains(sTestCaseID, Constants.Col_TestCaseID, Constants.Sheet_TestSteps);
				iTestLastStep = ExcelUtils.getTestStepsCount(Constants.Sheet_TestSteps, sTestCaseID, iTestStep);
				bResult=true;
				
				// Run step by step in the test case
				for(; iTestStep < iTestLastStep; iTestStep ++) {
		    		sActionKeyword = ExcelUtils.getCellData(iTestStep, Constants.Col_ActionKeyword,Constants.Sheet_TestSteps);
		    		sPageObject = ExcelUtils.getCellData(iTestStep, Constants.Col_PageObject, Constants.Sheet_TestSteps);
		    		sData = ExcelUtils.getCellData(iTestStep, Constants.Col_DataSet, Constants.Sheet_TestSteps);
		    		
		    		// Execute test step
		    		execute_Actions();
		    		
		    		// Set result FAIL for test case
					if(bResult==false) {
						ExcelUtils.setCellData(Constants.KEYWORD_FAIL,iTestcase,Constants.Col_Result,Constants.Sheet_TestCases);
						Log.endTestCase(sTestCaseID);
						break;
					}						
				}
				
				// Set result TRUE for test case
				if(bResult==true) {
					ExcelUtils.setCellData(Constants.KEYWORD_PASS,iTestcase,Constants.Col_Result,Constants.Sheet_TestCases);
					Log.endTestCase(sTestCaseID);	
				}					
			}
		}
    }	
    
    // Execute Keyword
    private static void execute_Actions() throws Exception {
		for(int i=0; i < method.length; i++) {
			if(method[i].getName().equals(sActionKeyword)) {
				method[i].invoke(actionKeywords, sPageObject, sData);
				
				// Set result for test step
				if(bResult==true) {
					ExcelUtils.setCellData(Constants.KEYWORD_PASS, iTestStep, Constants.Col_TestStepResult, Constants.Sheet_TestSteps);
					break;
				} else {
					ExcelUtils.setCellData(Constants.KEYWORD_FAIL, iTestStep, Constants.Col_TestStepResult, Constants.Sheet_TestSteps);
					ActionKeywords.closeBrowser("","");
					break;
				}
			}
		}
    }
 
}